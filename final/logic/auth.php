<?php
require_once 'db.php';

$login = trim($_POST['login']);
$pwd = trim($_POST['pwd']);

if( !empty($login) && !empty($pwd)){
    $sql = 'SELECT login, password FROM user WHERE login = :login';
    $params = [':login' => $login];

    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);

    $user = $stmt->fetch(PDO::FETCH_OBJ);

    if($user){
        if(password_verify($pwd, $user->password)){
            $_SESSION['user_login'] = $user->login;
            header('Location: ../index.php');
        }
        else{
            echo 'Login or password not correct';
        }
    }else{
        echo 'Login or password not correct';
    }
} else{
    echo "Please, input all data";
}