<?php
/**
 * Created by PhpStorm.
 * User: Vita
 * Date: 28.06.2018
 * Time: 20:02
 */

$food_id = $_POST['added_id'];
$cookie_name = "added[$food_id]";

if( isset($_COOKIE['added']) && array_key_exists($food_id, $_COOKIE['added'])){
    setcookie($cookie_name,'',time()-400,'/');
}else{
    setcookie($cookie_name,'1',time()+60*60, '/');
}