<?php
require_once 'db.php';

$sql = 'SELECT * FROM food';

$result = $pdo->query($sql);

while($food = $result->fetch(PDO::FETCH_OBJ)): ?>
<div class="food_container" id="<?php echo 'food_' . $food->food_id?>">
    <h3 class="food_name">Name: <?php echo $food->name ?></h3>
    <h4 class="food_description">Description: <?php echo $food->description ?></h4>
    <h4 class="food_price">Price: <?php echo $food->price ?></h4>

    <?php if(isset($_COOKIE['added']) && array_key_exists($food->food_id, $_COOKIE['added'])): ?>
        <button class="food_added food_added_active">Added to cart</button>
    <?php else: ?>
    <button class="food_added ">Add to cart</button>
    <?php endif; ?>
    <hr>
</div>

<?php endwhile; ?>