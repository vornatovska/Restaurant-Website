<?php

require_once 'db.php';

$login = trim( $_POST['login']); // trim - delete space from start and end of line
$pwd = trim( $_POST['pwd']);

if( !empty($login) && !empty($pwd)){

    $sql_check = 'SELECT EXISTS( SELECT login FROM user WHERE login = :login)';
    $stmt_check = $pdo->prepare($sql_check);
    $stmt_check->execute([':login' => $login]);

    if( $stmt_check -> fetchColumn()){
        die('we already have a user with this username');
    }

    $pwd = password_hash($pwd, PASSWORD_DEFAULT);
    $sql = 'INSERT INTO user(login, password) VALUES (:login, :pwd)';
    $params = ['login' => $login, ':pwd' => $pwd]; //bindParam

    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);

    echo 'Sign up is successful';
}else{
    echo "Please, input all data";
}
?>
<br>
<a href="../signin.php">Sign in</a>
