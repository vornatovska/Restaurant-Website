<?php require_once 'logic/db.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once 'parts/head.php'; ?>
</head>
<body>
<?php include_once 'parts/header.php'; ?>
<?php if(isset($_SESSION['user_login'])):?>

    <h3>Your CART:</h3>
        <?php
            if(isset($_COOKIE['added'])):
                echo $_COOKIE['added'];
            else:
                echo "Empty";
            endif;
        ?>
    <section id="menu-section">
        <?php include_once 'logic/print_menu.php'; ?>
    </section>
<?php else: ?>
    <?php include_once 'parts/not_auth.php'?>
<?php endif; ?>
</body>
</html>